const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: './src/app.ts',
	devtool: 'inline-source-map',
	mode: 'development',
	output: {
		filename: './dist/bundle.js',
		path: __dirname
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: 'ts-loader',
				exclude: /node_modules/
			}, {
				test: /\.hbs$/,
				loader: "handlebars-loader",
				exclude: /node_modules/
			}
		]
	},
	resolve: {
		extensions: [".tsx", ".ts", ".js"]
	},
	devServer: {
		contentBase: './dist'
	},
	// plugins: [
	// 	new HtmlWebpackPlugin({
	// 		title: 'Custom template using Handlebars',
	// 		filename: './dist/index.html',
	// 		template: './src/index.html'
	// 	})
	// ]
};