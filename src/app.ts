import {Game} from './classes/Game';
import {LoadScene} from "./classes/scenes/LoadScene";
import {GameScene} from "./classes/scenes/GameScene";

let config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: { y: 200 }
		}
	},
	scene: [LoadScene, GameScene]
};

let game = new Game(config);

