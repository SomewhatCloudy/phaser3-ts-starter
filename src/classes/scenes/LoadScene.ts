import "phaser";

/**
 *
 */
export class LoadScene extends Phaser.Scene {

	//
	private loadingBar: Phaser.GameObjects.Graphics;
	private progressBar: Phaser.GameObjects.Graphics;

	/**
	 *
	 */
	constructor() {
		super({
			key: "LoadScene"
		});
	}

	/**
	 *
	 */
	preload(): void {

		let self = this;

		this.cameras.main.setBackgroundColor(0x98d687);
		this.createLoadingbar();

		// pass value to change the loading bar fill
		this.load.on(
			"progress",
			function(value) {
				this.progressBar.clear();
				this.progressBar.fillStyle(0xfff6d3, 1);
				this.progressBar.fillRect(
					this.cameras.main.width / 4,
					this.cameras.main.height / 2 - 16,
					(this.cameras.main.width / 2) * value,
					16
				);
				console.log('Progress!', value);
			},
			this
		);

		// delete bar graphics, when loading complete
		this.load.on(
			"complete",
			function() {
				// this.progressBar.destroy();
				// this.loadingBar.destroy();
			},
			this
		);

		//
		this.load.pack("preload", "./assets/sprites.json", "preload");

	}

	update(): void {
		// this.add.image(200, 200, 'ship');
		this.scene.start("GameScene");
		console.log('Done!');
	}

	private createLoadingbar(): void {
		this.loadingBar = this.add.graphics();
		this.loadingBar.fillStyle(0x5dae47, 1);
		this.loadingBar.fillRect(
			this.cameras.main.width / 4 - 2,
			this.cameras.main.height / 2 - 18,
			this.cameras.main.width / 2 + 4,
			20
		);
		this.progressBar = this.add.graphics();
	}
}