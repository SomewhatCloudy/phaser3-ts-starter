import "phaser";
import {Ship} from "../sprites/Ship"

/**
 *
 */
export class GameScene extends Phaser.Scene {

	protected ship;

	/**
	 *
	 */
	constructor() {
		super({
			key: "GameScene"
		});
	}

	/**
	 *
	 */
	preload(): void {
		this.ship = new Ship({
			scene: this,
			x: 200,
			y: 400,
			key: "ship2_red"
		});
	}
}