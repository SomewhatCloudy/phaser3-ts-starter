import "phaser";

export class Ship extends Phaser.GameObjects.Sprite {

	private currentScene: Phaser.Scene;

	constructor(params) {
		super(params.scene, params.x, params.y, params.key);

		this.currentScene = params.scene;
		this.currentScene.add.existing(this);
	}
}